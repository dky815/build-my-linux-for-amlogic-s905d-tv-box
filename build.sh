#!/bin/bash

mkdir amlogic-bsp
cd amlogic-bsp

git clone --depth 1 git://git.yoctoproject.org/poky -b dunfell poky
git clone https://github.com/superna9999/meta-meson.git meta-meson -b dunfell

source poky/oe-init-build-env
bitbake-layers add-layer $PWD/../meta-meson/

export MACHINE=amlogic-s905d
bitbake amlogic-image-sato
