# build-my-linux-for-Amlogic-S905D-TV-box

This project is a GitLab CI/CD pipeline to build custom Linux system for Amlogic-S905D based on Pocky using Yocto.

For more details about the S905D SoC, please refer to the [Amlogic openlinux](http://openlinux.amlogic.com/Android/Mbox) official website.

The wic image can be downloaded from [job artifacts](https://gitlab.com/dky815/build-my-linux-for-amlogic-s905d-tv-box/-/jobs/4201318776/artifacts/browse/amlogic-bsp/build/tmp/deploy/images/amlogic-s905d/).

## Thanks
Reference distribution: [Poky](https://git.yoctoproject.org/poky?h=dunfell)

BSP layer for S905D: [meta-meson](https://layers.openembedded.org/layerindex/branch/master/layer/meta-meson/)

Community: [Linux for Amlogic](https://linux-meson.com/)